import React from "react";

const SponsorsLogo = (props) => {
      return (
            <div className="containre_logo">
                  <div className="sponor_logo_flex">
                        <img src={props.lg} className="lg_logo" />
                        <img src={props.coca} className="coca_logo" />
                        <img src={props.face} className="facebook" />
                        <img src={props.paypal} className="paypal" />
                        <img src={props.coca} className="coca_logo" />
                        <img src={props.shop} className="shop" />
                  </div>
            </div>
      )
}

export default SponsorsLogo
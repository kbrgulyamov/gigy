import React, { useEffect } from 'react';
import Aos from 'aos';
import "aos/dist/aos.css"

const Gigas = (props) => {
      useEffect(() => {
            Aos.init({ duration: 2000 })
      }, [])
      return (
            <div className="wrapper_block" data-aos="fade-up"
                  data-aos-duration="3000" >
                  <div className="title_g">
                        <h1 className="top" twoblock={props.r}>for gigas</h1>
                        <h1 className="down_h">Enhanced job and gig search tailor made for you.</h1>
                  </div>
                  <div className="wrap_block_item_top" >
                        <div className="left_text">
                              <h1 className="text_block_h1">See the latest job and gig openings<br /> from verified companies.</h1>
                              <p>Gigy ensures the best opportunities matching your<br /> skills from verified companies get to you in time.</p>
                        </div>
                        <div className="right_block_flex">
                              <img src={props.block} alt="" className="block" />
                        </div>
                  </div>

                  <div className="two_blocks_wrapper">
                        <div className="blocks_one">
                              <div className="title_one_block">
                                    <h1>Set your availability and weigh<br /> your options.</h1>
                                    <h2>With a plethora of opportunities, you can<br /> comfortably take decisions that suit you.</h2>
                              </div>
                              <div className="img_one_block">
                                    <img src={props.one_img} alt="" />
                              </div>
                        </div>


                        <div className="blocks_one_two">
                              <div className="title_one_block">
                                    <h1>Find courses to help you stay current and better<br /> equipped to deliver the best quality consistently.</h1>
                                    <h2>Gigy helps keep you at the top of your game by helping you gain new<br /> skills or sharpen your skills.</h2>
                              </div>
                              <div className="img_one_block_two">
                                    <img src={props.img_big} className="shit" alt="" />
                              </div>
                        </div>
                  </div>

            </div>
      )
}

export default Gigas
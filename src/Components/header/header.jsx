import React, { useState, useEffect } from 'react';

const Navbar = (props) => {
      const [fixed, setFixed] = useState(false)

      useEffect(() => {
            window.addEventListener('scroll', () => {
                  if (window.screenY > 1100) {
                        setFixed(true)
                  } else {
                        setFixed(false)
                  }
            })
      }, [])

      const EventClick = () => {
            alert('hello shit')
      }
      return (
            <header className={`navbar ${fixed && "fixedHeader"}`}>
                  <div className='container_header'>
                        <img src={props.img} alt='logo' className='logo' />
                        <div className='link_next_page'>
                              <ul className='list' >
                                    <li><a href=''>About Us</a></li>
                                    <li><a href=''>Blog</a></li>
                                    <li><a href=''>Contact Us</a></li>
                              </ul>
                        </div>

                        <div className='right_btn'>
                              <p className='text'>Post a Job</p>
                              <button onClick={EventClick} className='btn_header'>Find a Job</button>
                              <div className='menue'>
                                    <img src={props.img_menu} className='menu_block' />
                              </div>
                        </div>
                  </div>
            </header>

      )
}

export default Navbar
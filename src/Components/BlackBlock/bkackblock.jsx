import React, { useEffect } from 'react';
import Aos from 'aos';
import "aos/dist/aos.css"

const ComeOnboard = (props) => {
    useEffect(() => {
        Aos.init({ duration: 2000 })
    }, [])
    return (
        <div className="block_black_wrapper">
            <div className="bg_img_block" >
                <div className='container_black'>
                    <div className="title_center_block" data-aos="fade-up"
                        data-aos-duration="3000">
                        <p>Come onboard with <span className="insaett_g">Gigy</span>, here’s why!</p>
                    </div>
                    <div className="item_info">
                        <div className="block_info_wrap" data-aos="fade-up"
                            data-aos-duration="3000">
                            <div className="img_top">
                                <img src={props.times} className='times' />
                            </div>
                            <div className="text_down_flex">
                                <p className="wrap_text">Flexible Work Hours</p>
                                <p className="down_text">The decision of when and where<br /> to work is up to you.</p>
                            </div>
                        </div>

                        <div className="block_info_wrap" data-aos="fade-up"
                            data-aos-duration="3000">
                            <div className="img_top">
                                <img src={props.suck} className='suck' />
                            </div>
                            <div className="text_down_flex">
                                <p className="wrap_text">Secure payment</p>
                                <p className="down_text">The decision of when and where<br /> to work is up to you.</p>
                            </div>
                        </div>

                        <div className="block_info_wrap" data-aos="fade-up"
                            data-aos-duration="3000">
                            <div className="img_top">
                                <img src={props.frieng} className='times' />
                            </div>
                            <div className="text_down_flex">
                                <p className="wrap_text">Remote friendly</p>
                                <p className="down_text">The decision of when and where<br /> to work is up to you.</p>
                            </div>
                        </div>
                        <div className="block_info_wrap" data-aos="fade-up"
                            data-aos-duration="3000">
                            <div className="img_top">
                                <img src={props.setting} className='times' />
                            </div>
                            <div className="text_down_flex">
                                <p className="wrap_text">Increase Productivity</p>
                                <p className="down_text">The decision of when and where<br /> to work is up to you.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ComeOnboard
import React, { useEffect } from 'react';
import Aos from 'aos';
import "aos/dist/aos.css"


const Cetting = () => {
      useEffect(() => {
            Aos.init({ duration: 2000 })
      }, [])
      return (
            <div className="block_black_wrap" data-aos="fade-up"
                  data-aos-duration="3000">
                  <div className="bg_down_img">
                        <div className="text_flex_center">
                              <h1 className="top_text_3">Getting Started with Gigy is easy</h1>
                              <h1 className="down_text_7">Find the right jobs, gigs and best applicants in 10 minutes!</h1>
                        </div>
                        <div className="button-flex di">
                              <button className="btn_one b">Find a Job</button>
                              <div className="flex_btn">
                                    <img src="http://localhost:3000/static/media/post.abbfdfa97c5b457d5c2e657bc689e416.svg" className="img_btn" />
                                    <p className="btn_two s">Post a Job</p>
                              </div>
                        </div>
                  </div>
            </div>
      )
}


export default Cetting
import React from "react";

const Wrap = (props) => {
      return (
            <div className="wrap_center_container" >
                  <div className="title_center">
                        <h1>Be truly flexible</h1>
                  </div>
                  <div className="wrap_text_center">
                        <h1>All jobs and gigs that fit your<br /> schedule in one place.</h1>
                  </div>
                  <div className="info_text_center">
                        <p>Gigy reinforces a new way to work so you can put your <br /> skills to use and live life on your own terms.</p>
                  </div>
                  <div className="button-flex">
                        <button className="btn_one">Find a Job</button>
                        <div className="flex_btn">
                              <img src={props.post_btn} className="img_btn" />
                              <p className="btn_two">Post a Job</p>
                        </div>
                  </div>
            </div>
      )
}


export default Wrap
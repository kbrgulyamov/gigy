import React from "react";

const BlockDown = (props) => {
      return (
            <div className="container_down_block" data-aos="fade-up"
                  data-aos-duration="3000">
                  <div className="flex_block">
                        <div className="left_block_text">
                              <div className="title-wrap-text">
                                    <h1>Thousands of diversely<br /> skilled people use <span className="gigy">Gigy</span> <br /> to find tailor made gigs.</h1>
                                    <p className="p">Don’t take out word for it, see what<br /> our platform users have to say about<br /> the product.</p>
                              </div>
                        </div>

                        <div className="right_block_img_carusel">
                              <div className="block_item_right" >
                                    <div className="block_container">

                                          <div className="left_img_block">
                                                <img className="img_block" src={props.img_girl} />
                                          </div>

                                          <div className="flex_right_text">
                                                <h1 className="block_wrap_text">The ease of use is one of the best things I like<br /> about Gigy. The gigas are eager to work and I<br /> am always satisfied delegating my work to<br /> them and getting good results.</h1>
                                          </div>
                                    </div>
                                    <div className="avtor_block">
                                          <h2>Marcus Walter</h2>
                                          <p>Elderly care-giver</p>
                                    </div>
                                    <div className="img_icon">
                                          <img src={props.icon_block} className='icon' />
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
      )
}

export default BlockDown
import './App.css';
import './Media.css'
import Wrap from './Components/WrapBlock/wrapBlock';
import SponsorsLogo from './Components/Sponsots/sponser';
import Navbar from './Components/header/header';
import logo from './public/logo2.svg'
import menu from './public/menu.svg'
import post from './public/post.svg'
import lglogo from './public/lg.svg'
import face from './public/face.svg'
import coca from './public/coca.svg'
import paypal from './public/paypal.svg'
import shop from './public/shopi.svg'
import BlockDown from './Components/BlockDownSponsors/BlockT';
import icon from './public/icon.svg'
import ComeOnboard from './Components/BlackBlock/bkackblock';
import icontime from './log/hour.svg'
import suck from './log/secure.svg'
import frieng from './log/frieng.svg'
import settings from './log/settings.svg'
import Gigas from './Components/ForgigasBlock/BlockFor';
import block from './log/shit.svg'
import oneImg from './log/one.svg'
import Comp from './Components/Companies/Compan';
import Cetting from './Components/BlockDownGigy/gigyblock';
import Footer from './Components/Footer/Footer';
import girlImg from './public/girl.svg'
import big_img from './public/big_img3.svg'
import wrap_img_block from './public/big_img4.svg'
import two_blocks from './public/big_img4.svg'
import React, { useEffect } from 'react';
import Aos from 'aos';
import "aos/dist/aos.css"


function App() {

  useEffect(() => {
    Aos.init({ duration: 2000 })
  }, [])
  return (
    <div className="container" >
      <div className='bg_main'>

        <Navbar img={logo} img_menu={menu} />
        <Wrap post_btn={post} />
        <SponsorsLogo lg={lglogo} coca={coca} face={face} paypal={paypal} shop={shop} />
        <BlockDown icon_block={icon} img_girl={girlImg} />
      </div>
      <div className='black_block_Come'>
        <ComeOnboard times={icontime} suck={suck} frieng={frieng} setting={settings} />
      </div>
      <div className='container_g'>
        <Gigas block={block} one_img={oneImg} img_big={big_img} />
        <Comp wrap_one_block={wrap_img_block} img_two_block={two_blocks} one_img={oneImg} />
        <Cetting />
      </div>
      <footer>

        <Footer logo_footer={logo} />
      </footer>

    </div>
  )
}

export default App;

